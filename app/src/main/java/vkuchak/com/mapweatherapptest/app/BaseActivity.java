package vkuchak.com.mapweatherapptest.app;

import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import vkuchak.com.mapweatherapptest.R;

/**
 * Created by vkluc on 01.10.2016.
 */
public class BaseActivity extends AppCompatActivity {

    private Toolbar toolbar;

    public void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    public void setupBackActivity() {
        if (toolbar != null) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    public void setToolbarColor(@ColorInt int color) {
        if (toolbar != null) {
            toolbar.setBackgroundColor(color);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                //window.setStatusBarColor(TileUtil.getSystemBarColorForToolbar(color));
            }
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
