package vkuchak.com.mapweatherapptest.app.data;

import vkuchak.com.mapweatherapptest.api.model.BaseWeatherResponse;
import vkuchak.com.mapweatherapptest.api.model.SearchResponse;

/**
 * Created by vkluc on 01.10.2016.
 */

public interface MyAppDataSource {
    interface WeatherInfoResponse{

        void onSuccess(BaseWeatherResponse baseWeatherResponse);

        void onError();
    }
    interface CitySearchResponse{

        void onSuccess(SearchResponse searchResponse);

        void onError();
    }
}
