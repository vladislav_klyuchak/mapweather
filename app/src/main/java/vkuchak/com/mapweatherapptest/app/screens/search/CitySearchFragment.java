package vkuchak.com.mapweatherapptest.app.screens.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vkuchak.com.mapweatherapptest.R;
import vkuchak.com.mapweatherapptest.api.model.BaseWeatherResponse;
import vkuchak.com.mapweatherapptest.api.model.SearchResponse;
import vkuchak.com.mapweatherapptest.app.data.MyAppDataSource;
import vkuchak.com.mapweatherapptest.app.data.MyAppRepository;
import vkuchak.com.mapweatherapptest.app.screens.map.MapsActivity;

/**
 * Created by vkluc on 01.10.2016.
 */
public class CitySearchFragment extends Fragment implements SearchView.OnQueryTextListener{

    protected RecyclerView recyclerView;
    protected View emptyView;
    protected ContactsAdapter mAdapter;
    private SearchView searchView;
    List<BaseWeatherResponse> baseWeatherResponses= new ArrayList<>();

    public static CitySearchFragment newInstance() {
        return new CitySearchFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mAdapter = new ContactsAdapter();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        recyclerView =(RecyclerView) rootView.findViewById(R.id.recyclerView);
        emptyView = rootView.findViewById(R.id.empty_view);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(rootView.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mAdapter.getFilter().filter(newText);
        return false;
    }




    public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> implements Filterable {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final BaseWeatherResponse baseWeatherResponse = baseWeatherResponses.get(position);
            //String cityName = baseWeatherResponses.get(position).getName();
            holder.mTextView.setText(baseWeatherResponse.getName());
            holder.mWeather.setText(baseWeatherResponse.getWeather().get(0).getDescription());
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), MapsActivity.class);
                    intent.putExtra("city", baseWeatherResponse);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return baseWeatherResponses.size();
        }

        @Override
        public Filter getFilter() {
            return new Filter() {

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    return new FilterResults();
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if(!TextUtils.isEmpty(constraint) && constraint.length() > 2) {
                        MyAppRepository.getInstance(getContext()).getListOfCitiesBySubString(constraint.toString().toLowerCase(), new MyAppDataSource.CitySearchResponse() {
                            @Override
                            public void onSuccess(SearchResponse searchResponse) {
                                if(searchResponse.getBaseWeatherResponses().size() >0){
                                    baseWeatherResponses = searchResponse.getBaseWeatherResponses();
                                    dataChanged();
                                }

                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }


                }
            };
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTextView;
            public TextView mWeather;
            public LinearLayout linearLayout;
            public ViewHolder(View view) {
                super(view);
                linearLayout = (LinearLayout) view.findViewById(R.id.root_view);
                mTextView =(TextView) view.findViewById(R.id.text);
                mWeather = (TextView) view.findViewById(R.id.textWeather);
            }
        }
    }

    private void dataChanged() {

        if(baseWeatherResponses.size() > 0 ){
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        mAdapter.notifyDataSetChanged();
    }
}
