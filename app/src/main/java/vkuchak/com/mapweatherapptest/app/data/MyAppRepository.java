package vkuchak.com.mapweatherapptest.app.data;

import android.content.Context;
import android.support.annotation.NonNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vkuchak.com.mapweatherapptest.api.client.API;
import vkuchak.com.mapweatherapptest.api.model.BaseWeatherResponse;
import vkuchak.com.mapweatherapptest.api.model.SearchResponse;

/**
 * Created by vkluc on 30.09.2016.
 */

public class MyAppRepository implements MyAppDataSource {

    private static MyAppRepository INSTANCE = null;

    private Context mContext;

    public MyAppRepository(@NonNull Context context) {
        mContext = context;
    }

    public static MyAppRepository getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new MyAppRepository(context);
        }
        return INSTANCE;
    }


    public void getWeatherByCordinates(double lat, double lon, @NonNull final WeatherInfoResponse callback){
        API.get().getWeatherByGeographicCoordinates(lat, lon, API.MYAPPAPIID).enqueue(new Callback<BaseWeatherResponse>() {
            @Override
            public void onResponse(Call<BaseWeatherResponse> call, Response<BaseWeatherResponse> response) {
                BaseWeatherResponse baseWeatherResponse = response.body();
                if(baseWeatherResponse != null) {
                    callback.onSuccess(baseWeatherResponse);
                }
            }

            @Override
            public void onFailure(Call<BaseWeatherResponse> call, Throwable t) {
                callback.onError();
            }
        });
    }
    public void getListOfCitiesBySubString(String searchStr, @NonNull final CitySearchResponse callback ){
        String searchMod = "like";
        String outputMod = "json";
        API.get().getListOfCitiesBySubString(searchStr, searchMod, outputMod, API.MYAPPAPIID).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                SearchResponse searchResponse = response.body();
                if(searchResponse != null) {
                    callback.onSuccess(searchResponse);
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                callback.onError();
            }
        });
    }

}
