package vkuchak.com.mapweatherapptest.app.screens.search;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import vkuchak.com.mapweatherapptest.R;
import vkuchak.com.mapweatherapptest.app.BaseActivity;

/**
 * Created by vkluc on 01.10.2016.
 */

public class CitySearchActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_city_activity);
        setupToolbar();

        CitySearchFragment keywordsSearchFragment = (CitySearchFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        //long categoryId = getIntent().getLongExtra(EXTRA_CATEGORY, -1);
        if (keywordsSearchFragment == null) {
            keywordsSearchFragment = CitySearchFragment.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.contentFrame, keywordsSearchFragment);
            transaction.commit();
        }
    }
}
