package vkuchak.com.mapweatherapptest.app.utils;

import android.annotation.TargetApi;
import android.os.Build;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

/**
 * Created by vkluc on 18.08.2016.
 */
public class PermissionHelper {

    public interface GrantPermissionCallback {
        public void granted();

        public void denied();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void checkLocationPermission(final GrantPermissionCallback callback) {
        PermissionListener perm = new PermissionListener() {

            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                if (callback != null)
                    callback.granted();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                if (callback != null)
                    callback.denied();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        };

        if (!Dexter.isRequestOngoing())
            Dexter.checkPermission(perm, android.Manifest.permission.ACCESS_FINE_LOCATION);
    }
}
