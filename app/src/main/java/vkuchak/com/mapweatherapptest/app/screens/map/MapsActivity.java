package vkuchak.com.mapweatherapptest.app.screens.map;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import vkuchak.com.mapweatherapptest.R;
import vkuchak.com.mapweatherapptest.api.model.BaseWeatherResponse;
import vkuchak.com.mapweatherapptest.app.BaseActivity;
import vkuchak.com.mapweatherapptest.app.data.MyAppDataSource;
import vkuchak.com.mapweatherapptest.app.data.MyAppRepository;
import vkuchak.com.mapweatherapptest.app.screens.search.CitySearchActivity;
import vkuchak.com.mapweatherapptest.app.utils.PermissionHelper;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private UiSettings mMapSetting;
    //private List<Marker> listMarkers;
    // private DatabaseHelper databaseHelper;
    private Context mContext;
    //FloatingActionButton fab;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private FloatingActionButton fabMyLocation;
    boolean flagFabLocationIsClicked = false;
    BaseWeatherResponse cityFromSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mContext = this;

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        fabMyLocation = (FloatingActionButton) findViewById(R.id.btn_myLocation);
        setupToolbar();
        cityFromSearch = (BaseWeatherResponse) getIntent().getSerializableExtra("city");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_screen_menu, menu);

        final MenuItem item = menu.findItem(R.id.search);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(mContext, CitySearchActivity.class);
                startActivity(intent);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        mMapSetting = mMap.getUiSettings();
        mMapSetting.setMyLocationButtonEnabled(false);
        mMapSetting.setMapToolbarEnabled(false);
        isLocationPermission();

        if(cityFromSearch != null){
            setNewMarker(cityFromSearch.getCoordLatLeng(),
                    "Weather: " + cityFromSearch.getWeather().get(0).getMain()
                    + " more: " + cityFromSearch.getWeather().get(0).getDescription());
        }

        fabMyLocation.setVisibility(View.VISIBLE);
        fabMyLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setLocationFabSelected(true);
                    // add location API
                    Location location = mMap.getMyLocation();
                    if (location != null) {
                        LatLng target = new LatLng(location.getLatitude(), location.getLongitude());
                        // CameraPosition position = mMap.getCameraPosition();
                        setLocationWeather(target);
                        }

                }
            });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(final LatLng latLng) {
                if (latLng != null) {

                    setLocationFabSelected(false);
                    setLocationWeather(latLng);

                }
            }
        });
    }

    private void setLocationWeather(final LatLng latLng){
        MyAppRepository.getInstance(mContext).getWeatherByCordinates(latLng.latitude, latLng.longitude, new MyAppDataSource.WeatherInfoResponse() {
            @Override
            public void onSuccess(BaseWeatherResponse baseWeatherResponse) {
                if(baseWeatherResponse.getWeather().size() > 0) {
                    String weatherDescription = baseWeatherResponse.getWeather().get(0).getDescription();
                    String weatherMainInfo = baseWeatherResponse.getWeather().get(0).getMain();
                    Toast.makeText(mContext, "Weather: " + weatherMainInfo
                            + " more: " + weatherDescription, Toast.LENGTH_LONG).show();
                    setNewMarker(latLng, "Weather: " + weatherMainInfo
                            + " more: " + weatherDescription);
                }
            }

            @Override
            public void onError() {

            }
        });
    }
    private void setLocationFabSelected(boolean flag){
        int color;
        if(flag){
            flagFabLocationIsClicked = true;
            color = getResources().getColor(R.color.ripple_location_button);
        }else {
            color = getResources().getColor(R.color.colorAccent);
            flagFabLocationIsClicked= false;
        }

        fabMyLocation.setBackgroundColor(color);

    }

    void setNewMarker(LatLng latLng, String title){
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).title(title));
        CameraPosition.Builder builder = new CameraPosition.Builder();
        builder.zoom(15);
        builder.target(latLng);

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
       // mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }


    void isLocationPermission() {
        PermissionHelper.checkLocationPermission(new PermissionHelper.GrantPermissionCallback() {
            @Override
            public void granted() {
                mMap.setMyLocationEnabled(true);

            }

            @Override
            public void denied() {
                new AlertDialog.Builder(mContext)
                        .setTitle(R.string.informer_title_location_perm_need)
                        .setMessage(R.string.informer_message_location_perm_need)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                isLocationPermission();
                            }
                        })
                        .setNegativeButton(R.string.common_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        }).create().show();
            }
        });
    }

}
