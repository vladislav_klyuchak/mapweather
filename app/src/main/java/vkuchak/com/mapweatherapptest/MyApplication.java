package vkuchak.com.mapweatherapptest;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.karumi.dexter.Dexter;

/**
 * Created by vkluc on 18.08.2016.
 */
public class MyApplication extends Application {
    @Override public void onCreate() {
        super.onCreate();
        Dexter.initialize(this);

        if(BuildConfig.DEBUG){
            Stetho.initializeWithDefaults(this);
        }
    }
}
