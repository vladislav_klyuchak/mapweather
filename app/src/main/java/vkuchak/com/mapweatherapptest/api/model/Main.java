package vkuchak.com.mapweatherapptest.api.model;

import java.io.Serializable;

/**
 * Created by vkluc on 30.09.2016.
 *
 * "main":{"temp":293.14,"pressure":1016,"humidity":79,"temp_min":292.59,"temp_max":293.71},
 */
public class Main implements Serializable{
    private double temp;
    private double pressure;
    private int humidity;
    private double temp_min;
    private double temp_max;
}
