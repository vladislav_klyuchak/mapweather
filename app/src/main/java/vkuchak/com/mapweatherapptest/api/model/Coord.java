package vkuchak.com.mapweatherapptest.api.model;

import java.io.Serializable;

/**
 * Created by vkluc on 30.09.2016.
 *
 * "coord":{"lon":138.93,"lat":34.97}
 */
public class Coord implements Serializable {
    private double lon;
    private double lat;

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }
}
