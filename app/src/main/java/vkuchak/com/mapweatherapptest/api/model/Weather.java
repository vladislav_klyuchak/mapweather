package vkuchak.com.mapweatherapptest.api.model;

import java.io.Serializable;

/**
 * Created by vkluc on 30.09.2016.
 *
 * {"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}
 */

public class Weather implements Serializable{
    private int id;
    private String main;
    private String description;
    private String icon;

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }
}