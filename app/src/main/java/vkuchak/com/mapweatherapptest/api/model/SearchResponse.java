package vkuchak.com.mapweatherapptest.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vkluc on 01.10.2016.
 */

public class SearchResponse implements Serializable{
    private String massage;
    private int cod;
    private int count;
    private List<BaseWeatherResponse> list= new ArrayList<>();

    public int getCount() {
        return count;
    }

    public List<BaseWeatherResponse> getBaseWeatherResponses() {
        return list;
    }
}
