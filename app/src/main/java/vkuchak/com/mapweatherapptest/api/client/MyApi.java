package vkuchak.com.mapweatherapptest.api.client;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import vkuchak.com.mapweatherapptest.api.model.BaseWeatherResponse;
import vkuchak.com.mapweatherapptest.api.model.SearchResponse;

/**
 * Created by vkluc on 30.09.2016.
 */

public interface MyApi {
    String METHOD_WEATHER_BY_COORDINATES = "data/2.5/weather";
    String METHOD_FIND_CITY_BY_NAME = "data/2.5/find";

    String LATITUDE = "lat";
    String LONGITUDE = "lon";
    String APPID = "APPID";
    String SEARCH_SUBSTRING = "q";
    String TYPE_OF_SEARCH = "type";
    String RESULT_MOD = "mod";



    /**
     * Get weather by geographic coordinates
     *<p>
     *     Example Call
     *    @see <a href="api.openweathermap.org/data/2.5/weather?lat=35&lon=139&APPID=895ae2ba61403138df58425aaff27f0b">
     *        http://api.openweathermap.org/data/2.5/weather?lat=35&lon=139&APPID=895ae2ba61403138df58425aaff27f0b</a>
     * <p>
     *     Responce
     * <p>
     *     Fail:
     * <p>
     *     "cod": "404",
     *     "message": "Error: Not found city"
     * <p>
     *     Ok:
     * <p>
     *     {"coord":{"lon":138.93,"lat":34.97},
     *     "weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],
     *     "base":"stations",
     *     "main":{"temp":293.14,"pressure":1016,"humidity":79,"temp_min":292.59,"temp_max":293.71},
     *     "wind":{"speed":1.54,"deg":22,"gust":4.11},
     *     "rain":{"3h":0.565},"clouds":{"all":92},"dt":1475261731,
     *     "sys":{"type":3,"id":9469,"message":0.0049,"country":"JP","sunrise":1475181527,"sunset":1475224120},
     *     "id":1851632,
     *     "name":"Shuzenji",
     *     "cod":200}
     *     @param lat coordinates of the location of your interest
     *     @param lon coordinates of the location of your interest
     *
     */

    @GET(METHOD_WEATHER_BY_COORDINATES)
    Call<BaseWeatherResponse> getWeatherByGeographicCoordinates(
            @Query(LATITUDE) double lat,
            @Query(LONGITUDE) double lon,
            @Query(APPID) String appId
    );

    /**
     * Get list of citys by search substring
     *<p>
     *     Example Call
     *    @see <a href="api.openweathermap.org/data/2.5/find?q=London&type=like&mode=json&appid=895ae2ba61403138df58425aaff27f0b&id=2172797">
     *        http://api.openweathermap.org/data/2.5/find?q=London&type=like&mode=json&appid=895ae2ba61403138df58425aaff27f0b&id=2172797</a>
     * <p>
     *     Responce
     * <p>
     *     Fail:
     * <p>
     *
     * <p>
     *     Ok:
     * <p>
     *     {"message":"like","cod":"200","count":8,
     *     "list":[{"id":2643743,"name":"London","coord":{"lon":-0.12574,"lat":51.50853},"main":{"temp":287.65,"humidity":82,"pressure":1004.8,"temp_min":286.48,"temp_max":289.05},"dt":1475328049,"wind":{"speed":3.56,"deg":112},"sys":{"country":"GB"},"rain":{"1h":0.51},"clouds":{"all":100},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}]},{"id":7535661,"name":"London Borough of Harrow","coord":{"lon":-0.33333,"lat":51.566669},"main":{"temp":287.57,"humidity":83,"pressure":1004.9,"temp_min":286.48,"temp_max":288.95},"dt":1475327703,"wind":{"speed":4.21,"deg":116},"sys":{"country":"GB"},"rain":{"1h":0.51},"clouds":{"all":92},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}]},{"id":2643734,"name":"Londonderry County Borough","coord":{"lon":-7.30917,"lat":54.997211},"main":{"temp":285.197,"temp_min":285.197,"temp_max":285.197,"pressure":1017.16,"sea_level":1024.54,"grnd_level":1017.16,"humidity":87},"dt":1475327829,"wind":{"speed":3.41,"deg":18.0002},"sys":{"country":"GB"},"clouds":{"all":36},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03d"}]},{"id":6058560,"name":"London","coord":{"lon":-81.23304,"lat":42.983391},"main":{"temp":285.93,"humidity":96,"pressure":986,"temp_min":285.93,"temp_max":285.93},"dt":1475327944,"wind":{"speed":4.11,"gust":4.11,"deg":112},"sys":{"country":"CA"},"rain":{"3h":1.32},"clouds":{"all":88},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}]},{"id":4298960,"name":"London","coord":{"lon":-84.08326,"lat":37.128979},"main":{"temp":285.397,"temp_min":285.397,"temp_max":285.397,"pressure":989.52,"sea_level":1031.47,"grnd_level":989.52,"humidity":84},"dt":1475328222,"wind":{"speed":2.31,"deg":181.5},"sys":{"country":"US"},"clouds":{"all":88},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}]},{"id":4517009,"name":"London","coord":{"lon":-83.44825,"lat":39.886452},"main":{"temp":287.88,"humidity":89,"pressure":1016,"temp_min":285.93,"temp_max":291.48},"dt":1475328009,"wind":{"speed":2.01,"deg":128},"sys":{"country":"US"},"rain":{"3h":0.035},"clouds":{"all":8},"weather":[{"id":800,"main":"Clear","description":"Sky is Clear","icon":"02d"}]},{"id":5088905,"name":"Londonderry","coord":{"lon":-71.373947,"lat":42.865089},"main":{"temp":284.48,"humidity":96,"pressure":1027,"temp_min":283.15,"temp_max":286.48},"dt":1475327723,"wind":{"speed":2.96,"deg":48.5002},"sys":{"country":"US"},"rain":{"1h":0.61},"clouds":{"all":92},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}]},{"id":4361094,"name":"Londontowne","coord":{"lon":-76.549408,"lat":38.933449},"main":{"temp":288.88,"humidity":94,"pressure":1019,"temp_min":287.95,"temp_max":290.37},"dt":1475327720,"wind":{"speed":1.03,"gust":3.08,"deg":129},"sys":{"country":"US"},"rain":{"1h":0.73},"clouds":{"all":76},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}]}]}
     *
     *     @param searchWord search substring
     *     @param typeOfSearch can be: like - close result, accurate - accurate result
     *     @param typeOfOutput format of output info can be JSON, XML etc.
     *
     */
    @GET(METHOD_FIND_CITY_BY_NAME)
    Call<SearchResponse> getListOfCitiesBySubString(
            @Query(SEARCH_SUBSTRING) String searchWord,
            @Query(TYPE_OF_SEARCH) String typeOfSearch,
            @Query(RESULT_MOD) String typeOfOutput,
            @Query(APPID) String appId
    );

}
