package vkuchak.com.mapweatherapptest.api.model;

import java.io.Serializable;

/**
 * Created by vkluc on 30.09.2016.
 *
 * "sys":{"type":3,"id":9469,"message":0.0049,"country":"JP","sunrise":1475181527,"sunset":1475224120},
 */
public class Sys implements Serializable{
    private int type;
    private int id;
    private float message;
    private String country;
    private long sunrise;
    private long sunset;

}
