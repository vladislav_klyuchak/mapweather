package vkuchak.com.mapweatherapptest.api.client;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import vkuchak.com.mapweatherapptest.BuildConfig;

/**
 * Created by vkluc on 30.09.2016.
 */

public class API {

    private static MyApi MyApi;
    public static String MYAPPAPIID ="895ae2ba61403138df58425aaff27f0b";

    private String baseUrl = "http://api.openweathermap.org/";
    // TODO if have time icons for marker in map
    private static String IMG_URL = "http://openweathermap.org/img/w/";

    private API(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            StethoInterceptor stethoInterceptor = new StethoInterceptor();
            builder.addNetworkInterceptor(stethoInterceptor);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            builder.addNetworkInterceptor(interceptor);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MyApi = retrofit.create(MyApi.class);
    }

    public static MyApi get(){
        if(MyApi == null){
            new API();
        }
        return MyApi;
    }


    public String getBaseUrl() {
        return baseUrl;
    }
}
