package vkuchak.com.mapweatherapptest.api.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vkluc on 30.09.2016.
 */
// TODO change to Parcelable or create DB
public class BaseWeatherResponse implements Serializable {
    private Coord coord;
    private List<Weather> weather = new ArrayList<>();
    private String base;
    private Main main;
    private Wind wind;
    private Rain rain;
    private Sys sys;
    private int id;
    private String name;
    private int code;

    public List<Weather>  getWeather() {
        return weather;
    }

    public String getName() {
        return name;
    }

    public LatLng getCoordLatLeng() {
        return new LatLng(coord.getLat(), coord.getLon());
    }
}
