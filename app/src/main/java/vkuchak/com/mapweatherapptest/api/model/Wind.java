package vkuchak.com.mapweatherapptest.api.model;

import java.io.Serializable;

/**
 * Created by vkluc on 30.09.2016.
 *
 * "wind":{"speed":1.54,"deg":22,"gust":4.11}
 */

public  class Wind implements Serializable{
    private float speed;
    private float deg;
    private float gust;
}